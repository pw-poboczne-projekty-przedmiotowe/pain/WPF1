﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF1.Models 
{
    public class Book : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string title;
        public string Title
        {
            get { return title; }
            set { title = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Title")); }
        }

        private string author;
        public string Author
        {
            get { return author; }
            set { author = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Author")); }
        }

        private string genre;
        public string Genre
        {
            get { return genre; }
            set { genre = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Genre")); }
        }

        private DateTime publishDate;
        public DateTime PublishDate
        {
            get { return publishDate; }
            set { publishDate = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("PublishDate")); }
        }

        public Book(string title, string author, string genre, DateTime publishDate)
        {
            Title = title;
            Author = author;
            Genre = genre;
            PublishDate = publishDate;
        }
    }
}
