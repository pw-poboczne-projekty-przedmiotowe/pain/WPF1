﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF1.Models
{
    public class BooksModel
    {
        public ObservableCollection<Book> Books { get; private set; } = new ObservableCollection<Book>();
        public BooksModel()
        {
            var d1 = new DateTime(1899, 5, 1, 0,0,0);
            Books.Add(new Book("Holmes", "Aut1", "criminal", d1));
            var d2 = new DateTime(2010, 1, 2, 0,0,0);
            Books.Add(new Book("Tuf Wędrowiec", "George Martin", "sci-fi", d2));
        }
    }
}
