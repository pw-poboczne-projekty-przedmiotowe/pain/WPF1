﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF1.Views
{
    /// <summary>
    /// Logika interakcji dla klasy GenreSelector.xaml
    /// </summary>
    public partial class GenreSelector : UserControl
    {
        public GenreSelector()
        {
            InitializeComponent();
        }

        #region TextProperty
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(String), typeof(GenreSelector),
            new FrameworkPropertyMetadata(string.Empty, OnTextPropertyChanged, OnCoerceTextProperty));

        public string Text
        {
            get { return GetValue(TextProperty).ToString(); }
            set 
            {
                SetValue(TextProperty, value);
                update_image(); 
            }
        }

        private static void OnTextPropertyChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            GenreSelector control = source as GenreSelector;
            string text = (string)e.NewValue;
            control.IsText = text.Length > 0;
            control.update_image();
        }

        private static object OnCoerceTextProperty(DependencyObject sender, object data)
        {
            return data;
        }

        #endregion

        #region ImageSourceProperty
        public static readonly DependencyProperty ImageSourceProperty = DependencyProperty.Register("ImageSource", typeof(ImageSource), typeof(GenreSelector), new FrameworkPropertyMetadata(null));

        public ImageSource ImageSource
        {
            get { return GetValue(ImageSourceProperty) as ImageSource; }
            set { SetValue(ImageSourceProperty, value); }
        }
        #endregion


        #region readonly property
        private static readonly DependencyPropertyKey IsTextPropertyKey = DependencyProperty.RegisterReadOnly("IsText", typeof(bool), typeof(GenreSelector), new FrameworkPropertyMetadata(false));

        public static readonly DependencyProperty IsTextProperty = IsTextPropertyKey.DependencyProperty;

        public bool IsText
        {
            get { return (bool)GetValue(IsTextProperty); }
            private set { SetValue(IsTextPropertyKey, value); }
        }
        #endregion


        #region clickEvent
        public static readonly RoutedEvent ClickEvent = EventManager.RegisterRoutedEvent("Click", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(GenreSelector));

        public event RoutedEventHandler Click
        {
            add { AddHandler(ClickEvent, value); }
            remove { RemoveHandler(ClickEvent, value); }
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            change_selection();
            RaiseEvent(new RoutedEventArgs(ClickEvent));
        }
        #endregion

        private void change_selection()
        {
            string old_text = GetValue(TextProperty).ToString();
            Console.WriteLine(old_text);
            if (string.Equals(old_text, "sci-fi"))
            {
                SetValue(TextProperty, "criminal");
            }
            else if (string.Equals(old_text, "criminal"))
            {
                SetValue(TextProperty, "novel");
            }
            else
            {
                SetValue(TextProperty, "sci-fi");
            }
            update_image();
        }

        private void update_image()
        {
            Uri uri = null;
            string old_text = GetValue(TextProperty).ToString();

            if (string.Equals(old_text, "sci-fi"))
            {
                uri = new Uri("pack://application:,,,/Resources/scifi.png");
            }
            else if(string.Equals(old_text, "criminal"))
            {
                uri = new Uri("pack://application:,,,/Resources/crime.jpg");
            }
            else if (string.Equals(old_text, "novel"))
            {
                uri = new Uri("pack://application:,,,/Resources/novel.png");
            }
            SetValue(ImageSourceProperty, new BitmapImage(uri));
        }
    }
}
