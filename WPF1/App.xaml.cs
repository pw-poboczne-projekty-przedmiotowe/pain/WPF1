﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WPF1.Models;

namespace WPF1
{
    /// <summary>
    /// Logika interakcji dla klasy App.xaml
    /// </summary>
    public partial class App : Application
    {
        public MVVM.IWindowService WindowService { get; } = new MVVM.WindowService();
        private BooksModel BooksModel { get; } = new BooksModel();
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            ViewModels.BooksViewModel booksViewModel = new ViewModels.BooksViewModel(BooksModel);
            WindowService.Show(booksViewModel);
        }
    }
}
