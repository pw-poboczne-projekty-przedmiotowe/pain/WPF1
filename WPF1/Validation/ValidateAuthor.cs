﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WPF1.Validation
{
    class ValidateAuthor : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string str = value.ToString();
            if (! str.Contains(" "))
            {
                return new ValidationResult(false, "Imię i nawzisko autora powinny być rozdzielone spacją");
            }

            return ValidationResult.ValidResult;
        }
    }
}
