﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF1.MVVM;
using WPF1.Models;

namespace WPF1.ViewModels
{
    public class BookViewModel : MVVM.IViewModel
    {
        private BooksModel BooksModel { get; }
        private Book Book { get; }
        public Action Close { get; set; }

        public string Title { get; set; }
        public string Author { get; set; }
        public string Genre { get; set; }
        public DateTime PublishDate { get; set; }

        public RelayCommand<BookViewModel> OkCommand { get; } = new RelayCommand<BookViewModel>
           (
               (bookViewModel) => { bookViewModel.Ok(); }
           );
        public RelayCommand<BookViewModel> CancelCommand { get; } = new RelayCommand<BookViewModel>
            (
                (bookViewModel) => { bookViewModel.Cancel(); }
            );

        public BookViewModel(BooksModel booksModel, Book book)
        {
            BooksModel = booksModel;
            Book = book;
            if (Book != null)
            {
                Title = Book.Title;
                Author = Book.Author;
                Genre = Book.Genre;
                PublishDate = Book.PublishDate;
            }
            else
            {
                Title = "";
                Author = "";
                Genre = "novel";
                PublishDate = System.DateTime.Today;
            }
            
        }

        public void Ok()
        {
            if (Book == null)
            {
                Book book = new Book(Title, Author, Genre, PublishDate);
                BooksModel.Books.Add(book);
            }
            else
            {
                Book.Title = Title;
                Book.Author = Author;
                Book.Genre = Genre;
                Book.PublishDate = PublishDate;
            }
            Close?.Invoke();
        }
        public void Cancel() => Close?.Invoke();
    }
}
